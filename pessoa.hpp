#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>
using namespace std;
class Pessoa {
private:
   // Atributos
   string nome;
   string matricula;
   int idade;
   string sexo;
   string telefone;
public:
   // Métodos
   Pessoa();   // Construtor
   Pessoa(string nome, string telefone, int idade);
   ~Pessoa();  // Destrutor
   string getNome();          // Acessor Get
   void setNome(string nome); // Acessor Set
   string getMatricula();
   void setMatricula(string matricula);
   int getIdade();
   void setIdade(int idade);
   string getSexo();
   void setSexo(string sexo);
   string getTelefone();
   void setTelefone(string telefone);
   
   void imprimeDados();
};
#endif
