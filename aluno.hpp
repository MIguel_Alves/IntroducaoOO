#ifndef ALUNO_HPP
#define ALUNO_HPP

#include "pessoa.hpp"
#include <string>

// :: é sinal de escopo. Quer dizer que está dentro daquilo.
// : é sinal de herança e indica de onde será herdado

class Aluno : public Pessoa {
	//Atributos
private:
	float	ira;
	int semestre;
	string curso;

	//Métodos
public:
	Aluno();
	~Aluno();
	void setIra(float ira);
	float getIra();
	void setSemestre(int semestre);
	int getSemestre();
	void setCurso(string curso);
	string getCurso();

	void imprimeDadosAluno();
};

#endif